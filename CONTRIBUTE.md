# Contributing guidelines for users
[![Gitter](https://badges.gitter.im/goscommons-github-io/Lobby.svg)](https://gitter.im/goscommons-github-io/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
- Are you familiar with javascript
- Are able or willing to learn vuejs?
Join us! and contact us directly via our chat. 

# Guidelines to document issues properly
## Adding features
### New features as user stories:
We have already a quite ambitious backlog of features but at the moment, as well as feedback from users.
- If you want to open a request for a new feature as a user consider documenting it clearly with this format provided by [AlphaFounders](https://github.com/AlphaFounders/style-guide/edit/master/agile-user-story.md). Copy paste the template and start documenting your feature.
### Implementation ideas and description of features:
- Document your idea towards implementation if you happen to have a software background using the tempalte provided by [golang](https://github.com/golang/proposal/blob/master/design/TEMPLATE.md)
## Bug reports
[For bug report use this template](https://gist.github.com/carlo/3402842).
