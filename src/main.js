import Vue from "vue";
import App from "./App.vue";
import "./../node_modules/bulma/css/bulma.css";
import VueCodemirror from "vue-codemirror";

import 'codemirror/lib/codemirror.css';

Vue.config.productionTip = false;

new Vue({
    render: h => h(App)
}).$mount("#app");