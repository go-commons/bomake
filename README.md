# BOMAKE 👉[Test the tool](https://go-commons.gitlab.io/bomake)

### Simple and fast BOM Making compatible with git

![demo gif](demo.gif)

<!-- ### [👉 Watch the video tutorial]() -->

## Usage

Bomake is meant to be used in the context of open hardware documentation. The idea is that you have a yaml file with the specific format shown below. Yo can start right away by copying and pasting this code on a yaml file.

```
---                              # This is how you start a new component/item
bomName: Name of your BOM        # Name your bom in the first item
i: Name of item
q: 10                            # Quantity of item
cp: 20                           # Cost per unit
url: paste url link

---                              # This is the end of an item
i: Another item
q: 10
cp: 80
url:

---                              # Always end each item like shown in this line

```

### Work with your favorite editor

1. Create a `.yml` file and open it with your favorite editor.
2. Copy the code above.
3. Press the load yaml button in the app and load your `newBOM.yml`
4. It is a plus that you have installed in your editor a package for yaml syntax so that you can have color coding and syntax error highlights.

### Downloading your BOM as a pdf

- Press the `Download PDF` button on the top. It might take sometime depending on the size of the file.

### Using the web editor

- You can also use the web editor, it has yaml syntax highlight and the table on the right loads reactively the code you are writing.
- You can get the yaml text on the code by copying and pasting the code. Simply stand on the code ditor and press`Ctrl+A`.

### BOM version control with git

BOMS can become difficult to manage, specially when you want to have different versions of vendors, change items etc. You can now easily do this with git and the yaml file.
Bomake is made to be easily integrated in git workflows. Doing version control with git using sheets is not easy and straightforward. Now you simply do commits, checks merges, conflict management, etc on to of `yourBOM.yml`.

## Examples

Use the examples to learn faster. Just go to the link belows and copy paste them on the web editor or on your local editor.

😄 You are welcome to add more examples to the list if you have used BOMAKE.

- [A Hypothetical BOM for a CNC table](https://gitlab.com/go-commons/bomake/raw/master/examples/Hypothetical-CNC-BOM.yaml)
