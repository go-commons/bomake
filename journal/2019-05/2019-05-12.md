#DONE:30 ## Reactive editor-compiler
- The editor needs to react to the onload of the yaml file.
  - yaml file is loaded in text area
  - `text` on `data`

#DONE:10 ## Component CodeMirror.vue
response from vue dev tools, issue in CodeMirror component
```
vue.runtime.esm.js?2b0e:619 [Vue warn]: Avoid mutating a prop directly since the value will be overwritten whenever the parent component re-renders. Instead, use a data or computed property based on the prop's value. Prop being mutated: "textCode"

```

This was solved by changing the codemirror coupling with its child
```
// Wrong approach: bidirectional data binding writing property directly
<codemirror v-model="textCode" :options="cmOptions"></codemirror>

// Correct approach
<codemirror
      ref="myCm"
      :value="textCode"
      :options="cmOptions"
      @ready="onCmReady"
      @focus="onCmFocus"
      @input="onCmCodeChange"
    ></codemirror>

```

[Reference: vue codemirror repository](https://github.com/surmon-china/vue-codemirror): 
