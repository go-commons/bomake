#Epics: ## Structured open hardware repo template

Make a package with the same concept like vue cli but for hardware.
In this concept the user gets a structured folder with all dependencies install including webtemplate,
the repo can also be configured so that pages are deployed.

## Features:
- Webtemplate to show off project with all the documentation features
- structure of folders and src for BOM, images etc. This will allow to present documentation in the template for the team easily
  - This will also allow to fetch data from repos to populate a database of components
- We can have also a bundle for component data, and bom data presentation
- Creating an instance of a BOM

#DONE:20 Page rendered has to react to code editor interaction
- in this context I have to read a string and remove string quotes, probably is better to put it to `js-yaml-pack`


#NextSteps:80 Towards shipping code and functionality
- Currency update
    - Character needs to changed to different currencies
    - Set conversion rate
    (config of the BOM should include also the name, creator, etc.)
-

#NextSteps:70 Comment properly codebase specially with regard to component dependency


#DONE:20 Independent scroll bars, currently
